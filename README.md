# README #

* Web Socket Example
* v0.0.1 pre-alpha

This project is ment to be an example of the Web Sockets feature.

### Directory structure ###

- __app__ *// Compiled server files*
- __assets__ *// Assets folder*
    - __img__ *// Unminified images*
    - __js__ *// Client javascript*
    - __sass__ *// SASS*
- __public__ *// Public frontend folder (after gulp)*
    - __img__ *// Minified images*
    - __css__ *// Minified styles*
    - __js__ *// Minified js*
- __src__ *// Server project files* 
- __views__ *// Client view files*

### How do I get set up? ###

* Just "npm install" it and run "gulp".

### Who do I talk to? ###

* Juan Marcos Rigoli (rigoli82@gmail.com).